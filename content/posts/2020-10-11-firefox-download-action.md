---
title: "firefox download action"
date: 2020-10-11
tags:
---
# Firefox download action 
### change pop-up menu when you download something 
on some downloads, you cant choose which behavior firefox shoul remember, for example on RAR files  
To change the default behavior on firefox regards to  
'rar, zip, mkv, avi, and so on  
you can edit the 'handlers.json' file in your firefox profile folder  

## the quote font sign is displayed mostly wrong, so dont copy-and-paste 


the default `handlers.json`  
`{"defaultHandlersVersion":{"en-US":4},"mimeTypes":{},"schemes":{"irc":{"stubEntry":true,"handlers":[null,{"name":"Mibbit","uriTemplate":"https://www.mibbit.com/?url=%s"}]},"ircs":{"stubEntry":true,"handlers":[null,{"name":"Mibbit","uriTemplate":"https://www.mibbit.com/?url=%s"}]},"mailto":{"stubEntry":true,"handlers":[null,{"name":"Yahoo! Mail","uriTemplate":"https://compose.mail.yahoo.com/?To=%s"},{"name":"Gmail","uriTemplate":"https://mail.google.com/mail/?extsrc=mailto&url=%s"}]}}}  `

edit the part `"mimeTypes":{}` add in the brakets
`"application/pdf":{"action":0,"extensions":["pdf"]},"application/octet-stream":{"action":0,"extensions":["mkv"]},"application/octet-stream":{"action":0,"extensions":["mp4"]},"application/octet-stream":{"action":0,"extensions":["avi"]},"application/octet-stream":{"action":0,"extensions":["rar"]},"application/octet-stream":{"action":0,"extensions":["zip"]},"application/octet-stream":{"action":0,"extensions":["7zip"]},"application/octet-stream":{"action":0,"extensions":["mp3"]}`

so it looks like 
`"defaultHandlersVersion":{"en-US":4},"mimeTypes":{"application/pdf":{"action":0,"extensions":["pdf"]},"application/octet-stream":{"action":0,"extensions":["mkv"]},"application/octet-stream":{"action":0,"extensions":["mp4"]},"application/octet-stream":{"action":0,"extensions":["avi"]},"application/octet-stream":{"action":0,"extensions":["rar"]},"application/octet-stream":{"action":0,"extensions":["zip"]},"application/octet-stream":{"action":0,"extensions":["7zip"]},"application/octet-stream":{"action":0,"extensions":["mp3"]}},"schemes":{"irc":{"stubEntry":true,"handlers":[null,{"name":"Mibbit","uriTemplate":"https://www.mibbit.com/?url=%s"}]},"ircs":{"stubEntry":true,"handlers":[null,{"name":"Mibbit","uriTemplate":"https://www.mibbit.com/?url=%s"}]},"mailto":{"stubEntry":true,"handlers":[null,{"name":"Yahoo!Mail","uriTemplate":"https://compose.mail.yahoo.com/?To=%s"},{"name":"Gmail","uriTemplate":"https://mail.google.com/mail/?extsrc=mailto&url=%s"}]}}}`
