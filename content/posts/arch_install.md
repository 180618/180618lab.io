---
title: Arch install
date: "2021-02-21"
---

loadkeys de-latin1
#  
cfdisk  
/dev/sda1	512M  
/dev/sda2	25G  
/dev/sda3	XXX  
#  
mkfs.fat -F32 /dev/sda1  
mkfs.btrfs /dev/sda2  
mkfs.btrfs /dev/sda3    
#  
# mounting
mount /dev/sda2 /mnt
#  
# basic packages
pacstrap /mnt base linux-lts linux-firmware  
#  
# fstab
genfstab -U /mnt >> /mnt/etc/fstab  
# 
arch-chroot /mnt  
#  
ln -sf /usr/share/zoneinfo/Europe/Berlin /etc/localtime
#  
hwclock --systohc
#  
# install editor
pacman -S vim networkmanager grub efibootmgr sudo
systemctl enable NetworkManager
# Edit /etc/locale.gen and uncomment en_US.UTF-8 UTF-8 and other needed locales.
vim /etc/locale.gen
# de_DE.UTF-8 and en_US.UTF-8  
locale-gen  
#  
vim /etc/locale.conf  
LANG=de_DE.UTF-8  
#  
vim /etc/vconsole.conf  
KEYMAP=de-latin1  
#  
echo myhostname > /etc/hostname  
#  
vim /etc/hosts  
127.0.0.1 localhost  
::1 localhost  
127.0.1.1	myhostname.localdomain myhostname  
# if static ip add line of inet
# if btrfs is used install btrfs-prog
pacman -S btrfs-prog
# initramfs  
mkinitcpio -P
#  
# creating user instedt of root  
useradd -m -G wheel username  
passwd username
# and user to group
groupadd wheel  
gpasswd -a username wheel  
# visudo  
# uncomment wheel group for sudo access  
EDITOR=vim visudo
#   
mkdir /boot/EFI (if doing UEFI)  
mount /dev/sda1 /boot/EFI  #Mount FAT32 EFI partition (if doing UEFI)  
grub-install --target=x86_64-efi --bootloader-id=grub_uefi --recheck  
#  
# edit grub config if timer should 0
vim /etc/default/grub  
#  
grub-mkconfig -o /boot/grub/grub.cfg  
#  
# exit && boot into new os
exit
umount -R /mnt
reboot now
  
# 
# new os
loadkeys de-latin1
# install 
pacman xorg-server xfce4 xfce4-goodies lightdm-gtk-greeter reflector pulseaudio pavucontrol  

# start lightdm  
systemctl enable lightdm  

# update mirrors
sudo reflector --latest 20 --protocol https --country germany --sort rate --save /etc/pacman.d/mirrorlist

# after start edit 
# enabel autologin in lightdm
sudo vim /etc/lightdm/lightdm.conf  
[Seat:*]  
autologin-user=username  
#  
groupadd -r autologin  
gpasswd -a username autologin  
