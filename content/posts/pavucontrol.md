---
title: pavucontrol - set default audio - keybinding
date: "2020-10-28"
tags:
- os
- audio
- pacmd
description:
---

## pavucontrol - set default audio - keybinding


# If you need a audio source been set after boot, make changes to    
    /etc/pulse/default.pa
  
at the line  
    #SET DEFAULT SINK/SOURCE
  
    pacmd set-default-sink 0  

or  
    pacmd set-default-sink 1
    
