---
title: "2020 09 15 Creating a Post"
date: 2020-09-15T09:43:30+02:00
tags: ["website", "hugo", "post"]
---

creating a post using own templates  
1. create an archetypes folder  
2. then create a `default.md` file with contains the basic strusture for your template  

---
    ---  
    title: "{{ replace .Name "-" " " | title }}"  
    date: {{ .Date }}  
    tags:  
    ---  
---
in homepage-root directory start with  
`hugo new /posts/<date-title>.md`  

now you can edit the new created post  
