---
title: "2020 09 23 ffmpeg Copy Just Video and Audio Stream"
date: 2020-09-23T13:27:28+02:00
tags: ["ffmpeg"]
---

just select first video and first audio stream... *`no subtitle`*  

> ffmpeg -i input.mkv -map 0:v -c:v copy -map 0:a -c:a copy outputfile.mkv  

*`cutting time`*, you have to set an time marker at which point it will be cut. Example at 0 hour 12 min 16 sec.  

> ffmpeg -i input.mkv -t 00:12:16 -map 0:v -c:v copy -map 0:a -c:a copy outputfile.mkv  

if you have to select *__all__* __audio__ and *__all__* __video streams__ `map` flag will be drop and just do  

> ffmpeg -i input.mkv -t 00:12:16 -c:v copy -c:a copy outputfile.mkv  

### selecting streams  

> ffmpeg -i input0 -i input1 `-map 1:v:4 -map 0:a:2 -map 0:s:16` -c copy outputfile.mkv  

the `-map` is set right before the outputfile and before the `-c copy`  

`-map 1:v:4` select the5th video stream from input1  
`-map 0:a:2` select the 3rd audio stream from input0  
`-map 0:s:16` select the 17th subtitle stream from input0  
