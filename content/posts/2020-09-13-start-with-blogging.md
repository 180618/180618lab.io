---
title: Start with Blogging
date: 2020-09-13
tags: ["config","gitlab"]
---

### Basic Sctructure
[Hugo](https://gohugo.io) is the static site generator  
Atom is an editor (optional)  
`pacman -Ss atom`

[GitLab](https://www.gitlab.com) is the hosting platform  
Markdown is the language to type  

---

### Hugo Commands
##### as a terminal app, it uses commands in the folder of the site
`hugo server`

generates the site and ca be viewed on localhost:1313

---

### Markdown
##### Front Matter
is the header of a post and is to use Metadata  
`yaml uses --- (3 dashes)` `and no "" (quotes)`

GitLab Front Matter Wiki [>](https://gohugo.io/content-management/front-matter/)

##### other Front Matter Metadata
```  
isCJKLanguage: true  
subtitle:  
draft: true/false  
```

##### useful md's
> `to show some code`  
> `are not possible`  
