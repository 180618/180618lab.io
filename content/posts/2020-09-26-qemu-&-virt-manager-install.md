---
title: "2020 09 26 QEMU und Virt-Manager"
date: 2020-09-23T13:27:28+02:00
tags: ["qemu"]
---

#### QEMU und Virt-Manager

Kompatibilität prüfen:  

> `grep -o 'vmx \ | svm' / proc / cpuinfo`  

Hinweis: Wenn nichts zurückkommt, überprüfen Sie das BIOS, um sicherzustellen, dass Virtualisierung aktiviert ist.  

1. Führen Sie den folgenden Terminalbefehl aus:  

> `sudo apt install qemu-kvm libvirt-clients libvirt-daemon-system bridge-utils libguestfs-tools genisoimage virtinst libosinfo-bin virt-manager`  


2. Fügen Sie Ihren Benutzer zu KVM-Gruppen hinzu  
> `$ sudo adduser $ USER libvirt $ sudo adduser $ USER libvirt-qemu`  