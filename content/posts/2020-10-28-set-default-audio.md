---
title: set default audio
date: "20201028"
tags:
- os
- audio
- pacmd
description:
---

# If you need an audio source been set after boot, make changes to    

    /etc/pulse/default.pa  

at the line   
> `#SET DEFAULT SINK/`  
pacmd set-default-sink 0    

or  
> pacmd set-default-sink 1  

