---
title: "2020 09 15 automount an android device"
date: 2020-09-15T09:43:30+02:00
tags: ["shell","android"]
---

found a sh script from youtuber [Brodie Robertson](https://www.youtube.com/user/OmegaDungeon)  
it's automounting an android device  
it's need `dmenu` `mtpfs`  
and it's based on Arch Wiki [MTP](https://wiki.archlinux.org/index.php/Media_Transfer_Protocol)    

    #!/bin/sh
    Dir=$HOME/cell/

    # Make the cell directory if it doesn't exist
    if [ ! -d "$Dir" ]; then
      mkdir "$Dir"
    fi

    # Select device
    Devices=$(simple-mtpfs -l)

    if [ ! -n "$Devices" ]; then
    	notify-send "No devices found"
    	exit
    else
      Device=$(echo "$Devices" | dmenu -p "Select device:" || notify-send "No device selected")
      Id=${Device%%:*}
      Name=${Device##*: }
    	if [ ! -d "$Dir$Name" ]; then
    		mkdir "$Dir$Name"
    	fi
    fi

    if [ ! -n "$(find "$Dir$Name" -maxdepth 0 -empty)" ]; then
      fusermount -u "$Dir$Name" && notify-send "Android Mount" "Device Unmounted"
    else
      simple-mtpfs --device "$Id" "$Dir$Name" && notify-send "Android Mount" "Device Mounted in $Dir$Name"
    fi
*source: [https://github.com/BrodieRobertson/scripts/blob/master/dmtpfs](https://github.com/BrodieRobertson/scripts/blob/master/dmtpfs)
