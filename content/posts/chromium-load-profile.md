---
title: chromium profile dir
date: "2021-02-21"
---

# um einen shortcut zu erstellen benötige ich den richtigen Pfad zum Profile Ordner
> ~/.config/chromium/

# dort sind dann entweder
> default

# oder
> Profile\ 1

# für exec benötige ich dann
> chromium --profile-directory=Default

