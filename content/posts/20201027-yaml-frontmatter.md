---
title: yaml front matter
date: "2020-10-27"
tags:
- yaml
- markdown
- basics
description: template for markdown
---

Da es bei Markdown verschiedene Sprachen gibt, hatte ich mich für yaml entschieden.  
Leider hatte ich nie mir eine Vorlage erstellt.
  
Auch habe ich festgestellt, dass einen Code Block zu erstellen es sinnvoller ist, wenn ich einen Tab am Anfang der Zeile setzen kann und es nicht mit einem Backtick in einem \`code\` einbetten sollte, wenn es sich über mehrere Zeilen erstreckt.  
  
    
     ---
     title: yaml front matter
     date: "2020-10-27"
     tags:
     - yaml
     - markdown
     - basics
     description: template for markdown
     ---
