---
title: "gitlab unfork project"
date: 2020-09-13
tags: ["config","gitlab"]
---

If the project is still in fork realtion to the original repository,  
you see a ` Forked from User/Repository`  
under the subtitle of the project title in the "Details" view.
