---
title: awk search
date: "2020-02-21"
tags:
- bash
- awk
- 
description:
---

# search for multiple pattern in one line and print it

    awk '/1080p/ && /clicknupload/ && /hevc/{print}'
    awk '/1080p/ && /clicknupload/ {print}'

# or print to file
    awk '/1080p/ && /clicknupload/ && /hevc/{print}' > 1.txt
