---
title: Debloat Samsung Mobile
date: 2020-09-11
tags: ["samsung", "mobile", "adb"]
---

### dev option enable on phone
#### list devices
` adb devices`

#### connect device
` adb shell`

#### enter shell comand to list specific package
` pm list package | grep '<package name>'`

#### this will remove /<package name/> from user 0
`pm uninstall -k --user 0 <package name>`

#### install after removing for user 0
`cmd package install-existing <package name>`
