---
title: "2020 09 23 Using Vim Plugins"
date: 2020-09-23T14:02:22+02:00
tags:
---

by default vim doesnt use plugins, obviously...  

but you can use them  

for example a "Markdown Preview" Plugin  

for that you have to install vim-plug [->](https://github.com/junegunn/vim-plug)  

1. Download the modul  
2. edit ~/.vimrc
3. add a plugin section 
> call plug#begin('~/.vim/plugged')  
> Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() }, 'for': ['markdown', 'vim-plug']}    
> call plug#end()  
4. save and exit. 

Then start a new vim file (*or file with vim*)  
and start the install process  
`:PlugInstall`  




