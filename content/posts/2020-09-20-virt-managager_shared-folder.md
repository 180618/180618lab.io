---
title: "2020 09 20 virt-manager shared folder"
date: 2020-09-20T23:23:55+02:00
tags:
---

### How to set up shared folders in virt-manager  

#### the host is the `physical` machine and the guest machine is the `virtual-machine`  
create a folder  
> root@host# mkdir /share  
> root@host# chmod 777 /share  

Afterwards shut down the guest if it’s running and attach the new filesystem in virt-manager:  
1. Switch the view to detail hardware view: View > Details  
2. Add a device (Gerät hinzufügen)  
3. select filesystem (Dateisystem auswählen)  
4. source path (/share in our case) and virtual target path (anything you like, I will go with /sharepoint)  
5. Driver (Treiber) = default  
6. Switch mode to Mapped if you need to have write access from the guest  
7. Confirm and start the VM again  

Now you can mount your shared folder from the VM:  

> root@guest# mkdir /share  
> root@guest# mount -t 9p -o trans=virtio /sharepoint /share  

Or permanently add it to `/etc/fstab` file:  
> root@guest# cat /etc/fstab  
> ...
> /sharepoint   /share    9p  trans=virtio,version=9p2000.L,rw    0   0  

forked from [Mgr. Josef Strzibny](https://nts.strzibny.name/how-to-set-up-shared-folders-in-virt-manager/)  
  
